const Bluebird = require('bluebird')
const MessageStore = require('@suchsoftware/proof-of-concept-message-store')
const { v4: uuid } = require('uuid')

const { config } = require('../lib')
const PostgresClient = require('../lib/postgres-client')

const postgresClient = PostgresClient({
  connectionString: config.env.messageStoreUrl
})
const messageStore = MessageStore({ session: postgresClient })

function buildMessage (category, type, traceId) {
  return {
    streamName: `${category}-${uuid()}`,
    message: {
      id: uuid(),
      type: type,
      data: { some: 'data' },
      metadata: {
        correlationStreamName: `correlation-${uuid()}`,
        replyStreamName: `reply-${uuid()}`,
        causationMessageStreamName: `causationStream-${uuid()}`,
        causationMessagePosition: 0,
        causationMessageGlobalPosition: 1,
        properties: {
          traceId
        }
      }
    }
  }
}

const traceId = uuid()

const messages = [
  // For correlation stream linking
  buildMessage('correlated', 'Correlated'),

  // For traceId organization
  buildMessage('traced1', 'Traced', traceId),
  buildMessage('traced2', 'Traced', traceId)
]

Bluebird.each(messages, m =>
  messageStore.write(m.streamName, m.message)
).finally(() => messageStore.stop())
