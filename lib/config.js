/*
The system makes heavy use of dependency injection.  This file wires up all the
dependencies, making use of the environment.
*/

const KnexClient = require('./knex-client')
const ConsumerPositionsApp = require('./app/consumer-positions')
const Log = require('./log')
const CategoriesApp = require('./app/categories')
const StreamsApp = require('./app/streams')
const TraceIdStreamsApp = require('./app/trace-id-streams')

function createConfig ({ env }) {
  const log = Log(env.logLevel)
  const knexClient = KnexClient({
    connectionString: env.databaseUrl
  })
  const messageStoreDb = KnexClient({ connectionString: env.messageStoreUrl })
  const consumerPositionsApp = ConsumerPositionsApp({ db: knexClient })
  const categoriesApp = CategoriesApp({ db: knexClient })
  const streamsApp = StreamsApp({ messageStoreDb })
  const traceIdStreamsApp = TraceIdStreamsApp({ db: knexClient })

  return {
    env,
    db: knexClient,
    consumerPositionsApp,
    log,
    categoriesApp,
    streamsApp,
    traceIdStreamsApp
  }
}

module.exports = createConfig
