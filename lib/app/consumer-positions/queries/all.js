const camelcaseKeys = require('camelcase-keys')

function All ({ db }) {
  return () => {
    return db.then(client => client('consumer_position')).then(camelcaseKeys)
  }
}

module.exports = All
