const express = require('express')

const All = require('./queries/all')

function Handlers ({ all, log }) {
  function consumerPositions (req, res) {
    return all().then(positions => {
      res.render('consumer-positions/templates/consumer-positions', {
        positions,
        context: req.context
      })
    })
  }

  return {
    consumerPositions
  }
}

function build ({ db }) {
  const all = All({ db })
  const handlers = Handlers({ all })

  const router = express.Router()

  router.route('/').get(handlers.consumerPositions)

  return { handlers, router }
}

module.exports = build
