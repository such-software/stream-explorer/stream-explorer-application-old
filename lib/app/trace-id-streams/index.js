const express = require('express')

const Get = require('./queries/get')

function Handlers ({ get }) {
  function stream (req, res) {
    return get(req.params.traceId).then(stream => {
      res.render('trace-id-streams/templates/stream', {
        stream,
        context: req.context
      })
    })
  }

  return {
    stream
  }
}

function build ({ db }) {
  const get = Get({ db })
  const handlers = Handlers({ get })

  const router = express.Router()

  router.route('/:traceId').get(handlers.stream)

  return { handlers, router }
}

module.exports = build
