const camelcaseKeys = require('camelcase-keys')

function Get ({ db }) {
  return () => {
    return db
      .then(client => client('trace_id_stream'))
      .then(camelcaseKeys)
      .then(rows => rows[0])
  }
}

module.exports = Get
