const camelcaseKeys = require('camelcase-keys')

function All ({ db }) {
  return () => {
    return db
      .then(client => client('category').orderBy('category'))
      .then(camelcaseKeys)
  }
}

module.exports = All
