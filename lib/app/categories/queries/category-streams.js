const camelcaseKeys = require('camelcase-keys')

function CategoryStreams ({ db }) {
  return (category, log) => {
    if (log) {
      log.trace({ category }, 'Querying category streams')
    }

    return db
      .then(client => client('stream').where('category', category))
      .then(camelcaseKeys)
  }
}

module.exports = CategoryStreams
