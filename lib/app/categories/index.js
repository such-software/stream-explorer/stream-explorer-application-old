const express = require('express')

const All = require('./queries/all')
const CategoryStreams = require('./queries/category-streams')

function Handlers ({ all, categoryStreams }) {
  function categories (req, res) {
    req.context.log.trace('Viewing all categories')

    return all().then(categories => {
      res.render('categories/templates/categories', {
        categories,
        context: req.context
      })
    })
  }

  function show (req, res) {
    const { category } = req.params

    req.context.log.trace({ category }, 'Viewing category')

    return categoryStreams(category, req.context.log).then(categoryStreams => {
      res.render('categories/templates/category', {
        category,
        categoryStreams
      })
    })
  }

  return {
    categories,
    show
  }
}

function build ({ db }) {
  const all = All({ db })
  const categoryStreams = CategoryStreams({ db })
  const handlers = Handlers({ all, categoryStreams })

  const router = express.Router()

  router.route('/:category').get(handlers.show)
  router.route('/').get(handlers.categories)

  return { handlers, router }
}

module.exports = build
