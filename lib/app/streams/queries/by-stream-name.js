const camelcaseKeys = require('camelcase-keys')

function ByStreamName ({ messageStoreDb }) {
  return streamName => {
    return messageStoreDb
      .then(client =>
        client
          .withSchema('message_store')
          .select('type', 'position', 'time')
          .from('messages')
          .where({ stream_name: streamName })
          .orderBy('position', 'ASC')
      )
      .then(camelcaseKeys)
  }
}

module.exports = ByStreamName
