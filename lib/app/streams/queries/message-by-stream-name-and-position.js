const camelcaseKeys = require('camelcase-keys')

function MessageByStreamNameAndPosition ({ messageStoreDb }) {
  return (streamName, position) => {
    return messageStoreDb
      .then(client =>
        client
          .withSchema('message_store')
          .select(
            'id',
            'stream_name',
            'type',
            'position',
            'time',
            'global_position',
            'metadata'
          )
          .from('messages')
          .where({ stream_name: streamName, position })
      )
      .then(camelcaseKeys)
      .then(rows => rows[0])
  }
}

module.exports = MessageByStreamNameAndPosition
