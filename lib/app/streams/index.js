const express = require('express')

const ByStreamName = require('./queries/by-stream-name')
const MessageByStreamNameAndPosition = require('./queries/message-by-stream-name-and-position')

function Handlers ({ byStreamName, messageByStreamNameAndPosition }) {
  function show (req, res) {
    const { streamName } = req.params

    return byStreamName(streamName).then(messages => {
      res.render('streams/templates/stream', {
        streamName,
        messages,
        context: req.context
      })
    })
  }

  function message (req, res) {
    const { streamName, position } = req.params

    return messageByStreamNameAndPosition(streamName, position).then(
      message => {
        res.render('streams/templates/message', {
          message,
          streamName,
          position
        })
      }
    )
  }

  return {
    show,
    message
  }
}

function build ({ messageStoreDb }) {
  const byStreamName = ByStreamName({ messageStoreDb })
  const messageByStreamNameAndPosition = MessageByStreamNameAndPosition({
    messageStoreDb
  })
  const handlers = Handlers({ byStreamName, messageByStreamNameAndPosition })

  const router = express.Router()

  router.route('/:streamName/:position').get(handlers.message)
  router.route('/:streamName').get(handlers.show)

  return { handlers, router }
}

module.exports = build
