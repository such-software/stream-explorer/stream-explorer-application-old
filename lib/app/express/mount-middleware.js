const express = require('express')
const { join } = require('path')

const attachLocals = require('./attach-locals')
const lastResortErrorHandler = require('./last-resort-error-handler')
const PrimeRequestContext = require('./prime-request-context')
const logRequest = require('./log-request')

/**
 * @description Mounts all the application-level middleware on our Express
 * application
 * @param {obect} app Express application
 */
function mountMiddleware (app, env, config) {
  const primeRequestContext = PrimeRequestContext({ log: config.log })
  app.use(lastResortErrorHandler)
  app.use(express.static(join(__dirname, '..', 'public'), { maxAge: 86400000 }))
  app.use(primeRequestContext)
  app.use(logRequest)
  app.use(attachLocals)
}

module.exports = mountMiddleware
