const uuid = require('uuid/v4')

function PrimeRequestContext ({ log }) {
  return (req, res, next) => {
    const traceId = uuid()
    req.context = {
      traceId,
      log: log.child({ traceId })
    }

    next()
  }
}

module.exports = PrimeRequestContext
