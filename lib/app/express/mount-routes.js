/**
 * @description Mounts application routes into the Express application
 * @param {object} app Express app on which to mount the routes
 * @param {object} config A config object will all the parts of the system
 */
function mountRoutes (app, config) {
  app.use('/categories', config.categoriesApp.router)
  app.use('/consumer-positions', config.consumerPositionsApp.router)
  app.use('/streams', config.streamsApp.router)
  app.use('/trace-id-streams', config.traceIdStreamsApp.router)
  app.use('/', config.consumerPositionsApp.router)
}

module.exports = mountRoutes
