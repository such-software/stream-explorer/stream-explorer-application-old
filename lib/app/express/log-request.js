function logRequest (req, res, next) {
  req.context.log.info({ path: req.originalUrl })

  next()
}

module.exports = logRequest
