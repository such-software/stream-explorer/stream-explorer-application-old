const Pino = require('pino')

function Log (logLevel) {
  const logger = Pino({ level: logLevel })

  return logger
}

module.exports = Log
